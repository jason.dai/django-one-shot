# Generated by Django 4.1.3 on 2022-12-01 20:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("todos", "0003_todoitem"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="todoitem",
            options={"verbose_name": "To Do Items"},
        ),
    ]
